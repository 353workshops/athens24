package main

import (
	"fmt"
	"runtime"
	"sync"
)

func main() {
	var wg sync.WaitGroup
	wg.Add(2)

	const size = 50
	fmt.Println("Starting")

	go func() {
		defer wg.Done()
		for count := 0; count < size; count++ {
			for r := 'a'; r <= 'z'; r++ {
				fmt.Printf("%c ", r)
			}
		}
	}()

	go func() {
		defer wg.Done()
		for count := 0; count < size; count++ {
			for r := 'A'; r <= 'Z'; r++ {
				fmt.Printf("%c ", r)
			}
		}
	}()

	fmt.Println("Waiting")
	wg.Wait()

	g := runtime.GOMAXPROCS(0)
	fmt.Printf("\nDone (%d cores)\n", g)
}
