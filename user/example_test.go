package user

import "fmt"

func ExampleNew() {
	u := New("elliot", "elliot@e-corp.com")
	fmt.Println(u)

	// Output:
	// &{elliot elliot@e-corp.com}
}
