package user

import "fmt"

// func New(name, email string) (User, error) {
// func New(name, email string) *User {
// func New(name, email string) (*User, error) {
func New(name, email string) *User {
	u := User{ // u is allocated on the heap
		Name:  name,
		Email: email,
	}

	return &u
}

func (u *User) Greet(other string) {
	fmt.Printf("Hi %s, I'm %s\n", other, u.Name)
}

// go build -gcflags=-m
// Shows escape analysis

type User struct {
	Name  string
	Email string
}
