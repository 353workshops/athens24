package main

import (
	"fmt"
	"net/http"
	"time"
)

/* Concurrency patterns
- Future (wait for result)
- Fan out
*/

func main() {
	answer := func() int {
		time.Sleep(time.Second)
		return 42
	}
	ch := Future(answer)

	// do other stuff
	v := <-ch
	fmt.Println("v:", v)

	size, err := yearSize(2022)
	fmt.Printf("2022: %d (%v)\n", size/(1<<20), err)
}

type result struct {
	url string // call context

	n   int64
	err error
}

func yearSize(year int) (int64, error) {
	const urlTemplate = "https://d37ci6vzurychx.cloudfront.net/trip-data/%s_tripdata_%d-%02d.parquet"
	vendors := []string{"yellow", "green"}

	ch := make(chan result)
	for _, vendor := range vendors {
		for mon := 1; mon <= 12; mon++ {
			url := fmt.Sprintf(urlTemplate, vendor, year, mon)
			// n, err := dlSize(url)

			// fan out
			go func(u string) {
				r := result{url: u}
				r.n, r.err = dlSize(u)
				ch <- r
			}(url)
		}
	}

	// collect results
	size := int64(0)
	for i := 0; i < 12*len(vendors); i++ {
		r := <-ch
		if r.err != nil {
			return 0, r.err
		}

		size += r.n
	}

	return size, nil
}

// FIXME: Add context for timeout
func dlSize(url string) (int64, error) {
	resp, err := http.Head(url)
	if err != nil {
		return 0, err
	}

	if resp.StatusCode != http.StatusOK {
		return 0, fmt.Errorf(resp.Status)
	}

	return resp.ContentLength, nil
}

// Exercise: support fn func() (T, error)
func Future[T any](fn func() T) chan T {
	ch := make(chan T, 1)

	go func() {
		ch <- fn()
	}()

	return ch
}
