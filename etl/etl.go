package main

import (
	"errors"
	"fmt"
	"io"
	"log"
)

type Data struct {
	Line string
}

type Victoria struct {
	Host string

	n int
}

// pointer receiver for consistency with Henry
// *Data allows users to re-use same value
// func (v *Victoria) Pull() (Data, error) {
func (v *Victoria) Pull(d *Data) error {
	v.n++

	if v.n > 10 {
		return io.EOF
	}

	d.Line = fmt.Sprintf("data #%d", v.n)
	log.Printf("Victoria: %s", d.Line)
	return nil
}

type Henry struct {
	Host string
}

// pointer receiver since h is mutated
// *Data for consistency with Victoria
func (h *Henry) Store(d *Data) error {
	log.Printf("Henry: %s", d.Line)
	return nil
}

type Puller interface {
	Pull(d *Data) error
}

func pull(p Puller, data []Data) (int, error) {
	/*
		for _, d := range data {
			v.Pull(&d)
		}
	*/
	for i := range data {
		err := p.Pull(&data[i])
		if errors.Is(err, io.EOF) {
			return i, nil
		}

		if err != nil {
			return i, err
		}
	}

	return len(data), nil
}

type Storer interface {
	Store(d *Data) error
}

func store(s Storer, data []Data) (int, error) {
	for i := range data {
		if err := s.Store(&data[i]); err != nil {
			return i, err
		}
	}

	return len(data), nil
}

func Copy(s Storer, p Puller, batchSize int) error {
	data := make([]Data, batchSize)

	for {
		i, err := pull(p, data)
		if i == 0 {
			break
		}

		if err != nil {
			return err
		}

		if _, err := store(s, data[:i]); err != nil {
			return err
		}
	}

	return nil
}

type William struct {
	Host string
}

func (w *William) Store(d *Data) error {
	log.Printf("William: %s", d.Line)
	return nil
}

func main() {
	v := Victoria{
		Host: "localhost:8000",
	}
	m := Henry{
		Host: "localhost:9000",
	}

	if err := Copy(&m, &v, 3); err != io.EOF {
		fmt.Println(err)
	}
}

/* Rules of thumb
- Start with concrete types
- Interfaces say what we need, not what we provide
- Small interfaces (up to 5 methods, 1 is best)
- Accept interfaces, return types
*/

/* Though exercise - sorting

type Sortable interface {
	Less(i, j int) bool
	Len() int
	Swap(i, j int)
}

func Sort(s Sortable) {
	// ...
}
*/
