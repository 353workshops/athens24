/*
	What is the sha1 signature of the uncompressed content of http.log.gz?

$ cat http.log.gz | gunzip | sha1sum
ef7dc39754fd23f7a1a8657e2ffda49edc49fff9 -
*/
package main

import (
	"compress/gzip"
	"crypto/sha1"
	"errors"
	"fmt"
	"io"
	"os"
	"strings"
)

func main() {
	fmt.Println(fileSHA1("http.log.gz"))
	fmt.Println(fileSHA1("sha1.go"))
}

type gzWrapper struct {
	*gzip.Reader // gzWrapper embeds *gzip.Reader
	f            *os.File
}

func (gz *gzWrapper) Close() error {
	return errors.Join(
		gz.Reader.Close(),
		gz.f.Close(),
	)
}

func openFile(fileName string) (io.ReadCloser, error) {
	// cat http.log.gz
	file, err := os.Open(fileName)
	if err != nil {
		return nil, err
	}

	if !strings.HasSuffix(fileName, ".gz") {
		return file, nil
	}
	// | guznip
	r, err := gzip.NewReader(file)
	if err != nil {
		return nil, err
	}
	return &gzWrapper{r, file}, nil
}

// cat http.log.gz | gunzip | sha1sum
// cat sha1.go | sha1sum
func fileSHA1(fileName string) (string, error) {
	r, err := openFile(fileName)
	if err != nil {
		return "", err
	}
	defer r.Close()

	w := sha1.New()
	if _, err = io.Copy(w, r); err != nil {
		return "", err
	}

	return fmt.Sprintf("%x", w.Sum(nil)), nil
}
