package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"net/http/pprof"
	"os"
)

type Server struct {
	db  *DB
	log *log.Logger
}

func (s *Server) searchHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet {
		s.log.Printf("ERROR: bad method - %s (%s)", r.Method, r.RemoteAddr)
		http.Error(w, "bad method", http.StatusMethodNotAllowed)
		return
	}

	query := r.URL.Query().Get("q")
	if query == "" {
		s.log.Printf("ERROR: missing query (%s)", r.RemoteAddr)
		http.Error(w, "missing query", http.StatusBadRequest)
		return
	}

	matches := s.db.Search(query)
	if len(matches) == 0 {
		s.log.Printf("ERROR: no matches for %q", query)
		http.Error(w, "no matches", http.StatusNotFound)
		return
	}

	s.log.Printf("INFO: %q - %d matches", query, len(matches))
	reply := make([]map[string]any, len(matches))
	for i, item := range matches {
		reply[i] = map[string]any{
			"title":   item.Title,
			"content": item.Content,
			"link":    item.Link,
		}
	}

	if err := json.NewEncoder(w).Encode(reply); err != nil {
		s.log.Printf("ERROR: can't encode - %s", err)
	}
}

func main() {
	s := Server{
		db:  NewDB(),
		log: log.New(os.Stdout, "[search] ", log.LstdFlags|log.Lshortfile),
	}

	mux := http.NewServeMux()
	mux.HandleFunc("/search", s.searchHandler)
	mux.HandleFunc("/debug/pprof/profile", pprof.Profile)

	const addr = ":8080"
	srv := http.Server{
		Addr:    addr,
		Handler: mux,
	}

	s.log.Printf("INFO: starting on %s", addr)
	if err := srv.ListenAndServe(); err != nil {
		fmt.Fprintf(os.Stderr, "error: %s", err)
		os.Exit(1)
	}
}
