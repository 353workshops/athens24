package main

import "strings"

type Item struct {
	Title   string
	Content string
	Link    string
}

type DB struct{}

func NewDB() *DB {
	return &DB{}
}

func (db *DB) Search(query string) []Item {
	var matches []Item
	for _, item := range dbData {
		// if strings.Contains(strings.ToLower(item.Content), strings.ToLower(query)) {
		if strings.Contains(item.Content, strings.ToLower(query)) {
			matches = append(matches, item)
		}
	}

	return matches
}

func init() {
	for i := range dbData {
		dbData[i].Content = strings.ToLower(dbData[i].Content)
	}
}
