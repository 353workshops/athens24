package main

import "fmt"

type player struct {
	name   string
	points int
}

func main() {
	players := []player{
		// Avengers
		{"Bruce", 11},   // Hulk
		{"Natasha", 19}, // Black widow
	}

	// Give 10 points bonus
	// value semantics "for" loop ("p" is a copy)
	/*
		for _, p := range players {
			p.points += 10
		}
	*/
	// pointer semantics "for" loop
	for i := range players {
		players[i].points += 10
	}
	fmt.Println(players)
}

// type Rune int32
// type Rune = int32 // type alias
// type User = user.User
