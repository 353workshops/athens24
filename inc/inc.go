package main

import (
	"fmt"
)

func main() {
	n := 41
	fmt.Printf("main: n = %d, addr = %p\n", n, &n)
	inc(&n)
	fmt.Printf("main: n = %d, addr = %p\n", n, &n)

	fmt.Println(n)
}

// before: n is passed by value
// n is passed by pointer (reference)
func inc(n *int) {
	fmt.Printf("inc : n = %d, addr = %p\n", *n, n)
	*n++ // read, modify, write
	fmt.Printf("inc : n = %d, addr = %p\n", *n, n)
}
