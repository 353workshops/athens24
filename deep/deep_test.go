package deep

import (
	"testing"
	"time"

	"github.com/stretchr/testify/require"
)

func TestRelu(t *testing.T) {
	n := 7
	out := Relu(n)
	require.Equal(t, n, out)
	out = Relu(-2)
	require.Equal(t, 0, out)

	fout := Relu(7.0)
	require.Equal(t, 7.0, fout)

	mout := Relu(time.February)
	require.Equal(t, time.February, mout)
}

func TestMax(t *testing.T) {
	_, err := Max([]int{})
	require.Error(t, err)
	_, err = Max[int](nil)
	require.Error(t, err)

	im, err := Max([]int{2, 3, 1})
	require.NoError(t, err)
	require.Equal(t, 3, im)

	fm, err := Max([]float64{2, 3, 1})
	require.NoError(t, err)
	require.Equal(t, 3.0, fm)

}
