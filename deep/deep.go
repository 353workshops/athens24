package deep

import "fmt"

// Max should err if values is empty
// otherwise return maximal value in values
// Bonus: Support strings
type Ordered interface {
	OrderedNum | ~string
}

func zero[T any]() (z T) {
	return
}

// comparable  is a built in constraint
// basically anything you can do == with

func Max[T Ordered](values []T) (T, error) {
	if len(values) == 0 {
		/*
			var zero T
			return zero, fmt.Errorf("Max on empty slice")
		*/
		return zero[T](), fmt.Errorf("Max on empty slice")
	}

	m := values[0]
	for _, v := range values[1:] {
		if v > m {
			m = v
		}
	}

	return m, nil
}

type OrderedNum interface {
	~int | ~float64
}

// What' in [] is a "type constraint"
func Relu[T OrderedNum](n T) T {
	if n < 0 {
		return 0
	}

	return n
}

// Go generics uses GC shape stenciling
// Two types have the same GC shape if their underlying type is the same
// All pointers have the same GC shape

/*
Always code as if the guy who ends up maintaining your code
will be a violent psychopath who knows where you live.
Code for readability.
    - Martin Golding
*/

/* Know type at runtime

func Inc[T int|float](n T) T  {
	switch any(n).(type) {
	case int:
		// ...
	case float64:
		// ...
	}
	return n + 1
}
*/
