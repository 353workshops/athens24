package main

import (
	"fmt"
	"sync"
	"sync/atomic"
	"time"
)

func main() {
	//var mu sync.Mutex
	//counter := 0
	counter := int64(0)

	var wg sync.WaitGroup
	for n := 0; n < 10; n++ {
		wg.Add(1)
		go func() {
			defer wg.Done()
			for i := 0; i < 1_000; i++ {
				/*
					mu.Lock()
					{
						counter++
					}
					mu.Unlock()
				*/
				atomic.AddInt64(&counter, 1)
				time.Sleep(time.Microsecond)
			}
		}()
	}
	wg.Wait()
	fmt.Println(counter)
}

// go run -race .
// Also build & test
