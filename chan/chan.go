package main

import (
	"fmt"
)

func main() {
	ch := make(chan string)
	fmt.Println("len:", len(ch), "cap:", cap(ch))

	go func() {
		ch <- "hi" // send
	}()
	msg := <-ch // receive
	fmt.Println(msg)

	go func() {
		defer close(ch)
		for i := 0; i < 3; i++ {
			ch <- fmt.Sprintf("msg #%d", i)
		}

	}()

	for msg := range ch {
		fmt.Println(msg)
	}

	/* What the for loop above does
	for {
		msg, ok := <-ch
		if !ok {
			break
		}
		fmt.Println(msg)
	}
	*/

	msg = <-ch // ch is closed
	fmt.Printf("empty: %q\n", msg)
	msg, ok := <-ch
	fmt.Printf("empty: %q (%v)\n", msg, ok)

	q := make(chan int, 1) // buffered channel to avoid goroutine leak
	go func() {
		q <- 7
	}()

	// nobody reads from q
}

// zero vs missing
// - channels (, ok)
// - maps (, ok)
// - marshaling (JSON)

/* Channel semantics
- Send/receive will block until opposite operation
	- Buffered channel with "n" capacity has "n" no blocking sends
- Receive from a closed channel will return zero value without blocking
- Send to a closed channel will panic
- Closing a closed channel will panic
- Send/receive to/from a nil channel will block forever
	- var ch chan int // ch is nil
*/
