package units

type Unit string

const (
	Centimeter Unit = "Centimeter"
	Inch       Unit = "Inch"
)

// 1 inch = 2.54 centimeters

type Value struct {
	Amount float64
	Unit   Unit
}

func (v Value) Add(other Value) Value {
	a := other.Amount

	if v.Unit != other.Unit {
		switch v.Unit {
		case Inch:
			a /= 2.54
		case Centimeter:
			a *= 2.54
		}
	}

	return Value{v.Amount + a, v.Unit}
}

/*
Pointer semantics
// v is called "the receiver"
// v is a pointer receiver
func (v *Value) Add(other Value) {
	a := other.Amount

	if v.Unit != other.Unit {
		switch v.Unit {
		case Inch:
			a /= 2.54
		case Centimeter:
			a *= 2.54
		}
	}

	v.Amount += a
}

*/

/* Rules of thumb
- Use value semantics as much as you can
- Pass pointers down the stack
- Use pointer receivers when you have mutating methods
- Value to pointer is OK (unmarshale), pointer to value is not
*/
